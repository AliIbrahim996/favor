<?php
require_once "../Init/RateInit.php";
header("Access-Control-Allow-Methods: POST");
$data = RateInit::getData();
$rate = RateInit::getRate();
if (Validation::checkEmptyRateData($data)) {
    echo $rate->createRate($data);
} else {
    http_response_code(204);
    echo json_encode(array(
        "message" => "check your data!",
        "flag" => 0
    ));
}