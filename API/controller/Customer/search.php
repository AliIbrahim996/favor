<?php
require_once "../Init/RestInit.php";
header("Access-Control-Allow-Methods: GET");
$data = RestInit::getData();
$rest = RestInit::getRestaurant();
if (!empty($data->query)) {
    echo $rest->search($data->query);
} else {
    http_response_code(204);
    echo json_encode(
        array("message" => "check your data!", "flag" => 1)
    );
}
